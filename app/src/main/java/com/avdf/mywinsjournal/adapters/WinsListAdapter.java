package com.avdf.mywinsjournal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avdf.mywinsjournal.R;
import com.avdf.mywinsjournal.model.Entry;
import com.avdf.mywinsjournal.utils.Helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Angelhia on 17/4/2017.
 */

public class WinsListAdapter extends BaseAdapter {

    Context context;
    List<Entry> winsParentList = new ArrayList<>();
    private LayoutInflater inflater;

    public WinsListAdapter(Context context, List<Entry> winsParentList) {
        this.context = context;
        this.winsParentList = winsParentList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return winsParentList.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolder;

        View vi = view;
        if (vi == null) {
            vi = inflater.inflate(R.layout.item_wins, viewGroup, false);

            viewHolder = new ViewHolderItem();
            viewHolder.date = (TextView) vi.findViewById(R.id.winsDate);
            viewHolder.title = (TextView) vi.findViewById(R.id.winsTitle);
            viewHolder.entry = (TextView) vi.findViewById(R.id.winsEntry);
            vi.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) view.getTag();
        }

        String createdDate =  winsParentList.get(i).getCreatedDate();
        viewHolder.date.setText(context.getResources().getString(R.string.label_posted_date) + createdDate);
        viewHolder.title.setText(winsParentList.get(i).getTitle());
        viewHolder.entry.setText(winsParentList.get(i).getWins());

        return vi;

    }

    @Override
    public Object getItem(int i) {
        return winsParentList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    static class ViewHolderItem {
        TextView date;
        TextView title;
        TextView entry;
    }
}
