package com.avdf.mywinsjournal.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.avdf.mywinsjournal.R;
import com.avdf.mywinsjournal.adapters.WinsListAdapter;
import com.avdf.mywinsjournal.model.Entry;
import com.avdf.mywinsjournal.utils.DialogHelper;
import com.avdf.mywinsjournal.utils.GlobalVariables;
import com.avdf.mywinsjournal.utils.Helper;
import com.avdf.mywinsjournal.utils.Settings;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private SwipeMenuListView winsListView;
    private List<Entry> winsList = new ArrayList<>();
    private TextView buttonAddNew;

    private Settings settings;

    //runtime permission
    protected String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    protected final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static int EMAIL_REQUEST_CODE = 1112;
    private static int PICKFILE_REQUEST_CODE = 1113;

    private String path = "";
    private String dateToday = "";

    int mode = 0; //1 = backup 2 = import

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);

        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        settings = new Settings(this);

        winsListView = (SwipeMenuListView) findViewById(R.id.winsListView);
        buttonAddNew = (TextView) findViewById(R.id.buttonAddNew);

        winsListView.setMenuCreator(creator);
        winsListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        winsListView
                .setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                        delete(position);
                        return false;
                    }
                });
        winsListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                winsListView.smoothOpenMenu(position);
            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });

        winsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                createNewEntry(winsList.get(i));
            }
        });


        buttonAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewEntry(null);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                createNewEntry(null);
                break;
            case R.id.action_backup:
                mode = 1;
                if (winsList.size() > 0) {
                    if (checkPermissions())
                        backUp();
                } else {
                    DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.erro_no_backup));
                }
                break;
            case R.id.action_import:
                mode = 2;
                if (checkPermissions())
                    openFileBrowser();
                break;
        }
        return true;
    }

    private void createNewEntry(Entry entry) {
        Intent intent = new Intent(this, CreateWinsActivity.class);
        if (entry != null)
            intent.putExtra(GlobalVariables.INTENT_EXTRA_ENTRY, entry);
        startActivity(intent);
    }

    private void backUp() {
        DialogHelper.showOkayDialog(MainActivity.this, "", getResources().getString(R.string.confirm_backup), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String toExport = "";
                toExport = Helper.convertListToString(winsList);
                if (toExport.equals("")) {
                    Log.d("error string", "string");
                    DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.error_backup));
                } else {
                    new Backup().execute(toExport);
                }
            }
        });
    }


/**
 * Handle exporting of scanned db to CSV
 */
class Backup extends AsyncTask<String, Boolean, Boolean> {
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogHelper.showProgressDialog(MainActivity.this, getResources().getString(R.string.label_saving));
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        return saveToLocalStorage(strings[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        DialogHelper.dismissPogressDialog();
        if (aBoolean) {
            DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.label_backup_success), getResources().getString(R.string.success_backup), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    shareViaEmail();
                }
            });

        } else
            DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.error_backup));
    }
}

    /**
     * Handle exporting of scanned db to local storage in .txt format
     *
     * @return
     */
    private boolean saveToLocalStorage(String text) {
        String root = Environment.getExternalStorageDirectory().toString();
        String myDir = root + GlobalVariables.SAVED_PATH;
        File dir = new File(myDir);
        if (!dir.exists())
            dir.mkdirs();

        File file;
        PrintWriter printWriter = null;
        try {
            dateToday = Helper.fileNameMaker();
            file = new File(dir, GlobalVariables.FILE_NAME + dateToday + ".txt");
            file.createNewFile();
            printWriter = new PrintWriter(new FileWriter(file));
            path = file.getAbsolutePath();
            printWriter.println(text);
        } catch (Exception exc) {
            exc.printStackTrace();
            return false;

        } finally {
            if (printWriter != null) printWriter.close();
        }

        return true;
    }

    /**
     * Handle sending of CSV file to email
     */
    private void shareViaEmail() {
        String recepients[] = {};
        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recepients);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.title_email) + " " + dateToday);
        emailIntent.setType("text/plain");

        //file attachment
        ArrayList<Uri> uris = new ArrayList<Uri>();
        uris.add(Uri.parse("file://" + path));

        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivityForResult(emailIntent, EMAIL_REQUEST_CODE);
    }

    private void openFileBrowser() {
        // Launch intent to pick file for upload
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }


    SwipeMenuCreator creator = new SwipeMenuCreator() {

        @Override
        public void create(SwipeMenu menu) {
            // create "delete" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(
                    MainActivity.this);
            // set item background
            deleteItem.setBackground(new ColorDrawable(getResources().getColor(android.R.color.white)));
            // set a icon
            int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
            deleteItem.setWidth(dimensionInDp);
            deleteItem.setIcon(R.drawable.ic_trash);
            // add to menu
            menu.addMenuItem(deleteItem);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        setList();
    }

    private void setList() {
        winsList = settings.getEntryList();

        if (winsList.size() == 0)
            buttonAddNew.setVisibility(View.VISIBLE);
        else
            buttonAddNew.setVisibility(View.GONE);

        WinsListAdapter adapter = new WinsListAdapter(this, winsList);
        winsListView.setAdapter(adapter);
    }

    private void delete(int pos) {
        String id = winsList.get(pos).getId();
        final String finalId = id;
        DialogHelper.showOkayDialog(this, getResources().getString(R.string.title_confirm_delete), getResources().getString(R.string.confirm_delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                settings.deleteEntry(finalId);
                setList();
            }
        });
    }

    protected boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(MainActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(MainActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mode == 1)
                        backUp();
                    else if (mode == 2)
                        openFileBrowser();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onRequest code", "here");
        if (requestCode == PICKFILE_REQUEST_CODE) {
            Log.d("onRequest code", "pick file");
            if (resultCode == RESULT_OK) {
                Log.d("onRequest code", "pick file ok");
                String text = "";
                if (resultCode == RESULT_OK) {
                    Uri test = data.getData();
                    String path = "";
                    try {
                        path = getFilePath(test);
                    } catch (URISyntaxException e) {
                    }

                    try {
                        if (path.contains(".txt")) {
                            InputStream stream = new FileInputStream(path);
                            text = Helper.convertStreamToString(stream);
                        }
                    } catch (IOException e) {
                        e.getMessage();
                        e.printStackTrace();
                    }

                    if (text.equals("")) {
                        DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.error_import));
                    } else {
                        List<Entry> newEntryList = new ArrayList<>();
                        newEntryList = Helper.convertStringToArrayList(text);
                        new InsertNewEntriesToDB().execute(newEntryList);
                    }

                }
            }
        }
    }

class InsertNewEntriesToDB extends AsyncTask<List<Entry>, String, String> {
    @Override
    protected void onPreExecute() {
        DialogHelper.showProgressDialog(MainActivity.this, getResources().getString(R.string.label_saving));
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(List<Entry>... lists) {
        insertToDB(lists[0]);
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        DialogHelper.dismissPogressDialog();
        DialogHelper.showOkayDialog(MainActivity.this, getResources().getString(R.string.success_import));
    }

}


    private void insertToDB(List<Entry> list) {
        settings.saveNewEntry(list);


    }


    @SuppressLint("NewApi")
    public String getFilePath(Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(MainActivity.this.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
