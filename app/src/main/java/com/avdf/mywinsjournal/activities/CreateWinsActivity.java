package com.avdf.mywinsjournal.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.EditText;

import com.avdf.mywinsjournal.R;
import com.avdf.mywinsjournal.model.Entry;
import com.avdf.mywinsjournal.utils.DialogHelper;
import com.avdf.mywinsjournal.utils.GlobalVariables;
import com.avdf.mywinsjournal.utils.Helper;
import com.avdf.mywinsjournal.utils.Settings;

public class CreateWinsActivity extends BaseActivity {

    EditText editTextTitle, editTextWins;

    Settings settings;

    Entry entry = null;

    String dateCreated = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_back);

        setContentView(R.layout.activity_createwins);

        settings = new Settings(this);

        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextWins = (EditText) findViewById(R.id.editTextWins);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            entry = (Entry) extras.getSerializable(GlobalVariables.INTENT_EXTRA_ENTRY);
        }

        if (entry != null) {
            editTextWins.setText(entry.getWins());
            editTextTitle.setText(entry.getTitle());
            dateCreated = entry.getCreatedDate();
        } else {
            dateCreated = Helper.getDate();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_wins_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_save:
                saveEntry();
                break;
        }
        return true;
    }

    private void saveEntry() {
        String title = editTextTitle.getText().toString();
        String wins = editTextWins.getText().toString();

        if (title.equals("") && wins.equals(""))
            return;

        Entry item = new Entry();
        item.setTitle(title);
        item.setWins(wins);
        item.setDate(Helper.getDate());
        item.setCreatedDate(dateCreated);

        if (entry != null)
            item.setId(entry.getId());
        else
            item.setId("");

        settings.saveEntry(item);

        finish();
    }

    @Override
    public void onBackPressed() {
        String title = editTextTitle.getText().toString();
        String wins = editTextWins.getText().toString();

        if (title.equals("") && wins.equals("") || !areThereChanges(title, wins))
            super.onBackPressed();
        else {
            DialogHelper.showOkayDialog(this, getResources().getString(R.string.title_confirm_discard), getResources().getString(R.string.confirm_discard), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
    }

    private boolean areThereChanges(String title, String wins) {
        if (entry != null) {
            if (!entry.getTitle().equals(title) || !entry.getWins().equals(wins)) {
                return true;
            }
        }

        return false;
    }

}
