package com.avdf.mywinsjournal.utils;

import android.content.Context;
import android.util.Log;

import com.avdf.mywinsjournal.database.DBHandler;
import com.avdf.mywinsjournal.model.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Angelhia on 12/6/2017.
 */

public class Helper {

    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(Calendar.getInstance().getTime());
        return date;
    }

    public static String fileNameMaker() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(Calendar.getInstance().getTime());
        return date;
    }


    public static String convertListToString(List<Entry> list) {
        String result = "";
        try {
            List<String> toConvertList = new ArrayList<>();
            toConvertList.add(GlobalVariables.FILE_HEADER);
            for (int i = 0; i < list.size(); i++) {
                String a = list.get(i).getTitle() + "," +
                        list.get(i).getWins() + "," +
                        list.get(i).getDate() + "," +
                        list.get(i).getCreatedDate();
                toConvertList.add(a);
            }
            result = android.text.TextUtils.join("\n", toConvertList);
        } catch (Exception e) {
        }
        Log.d("convertListToString", result);
        return result;
    }

    public static boolean isTextFileValid(String input) {
        Log.d("input", "input"+input);
        if (input.contains(GlobalVariables.FILE_HEADER)) {
            return true;
        }
        return false;
    }

    public static String convertStreamToString(InputStream stream) {
        String data = "";
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            data = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static List<Entry> convertStringToArrayList(String input) {
        List<Entry> entryList = new ArrayList<>();
        try {
            List<String> list = Arrays.asList(input.split("[\\r\\n]+"));
            for (int i = 1; i < list.size(); i++) {
                Entry entry = new Entry();
                String[] separated = list.get(i).split(",");
                entry.setTitle(separated[0]);
                entry.setWins(separated[1]);
                entry.setDate(separated[2]);
                entry.setCreatedDate(separated[3]);
                entryList.add(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entryList;
    }
}
