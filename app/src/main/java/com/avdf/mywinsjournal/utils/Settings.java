package com.avdf.mywinsjournal.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.avdf.mywinsjournal.database.DBHandler;
import com.avdf.mywinsjournal.model.Entry;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Angelhia on 12/6/2017.
 */

public class Settings {

    private Context context;

    private String APP_PREF = "com.avdf.mywinsjournal.sharedprefs";

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor prefEditor;

    DBHandler db;

    public Settings(Context context) {
        this.context = context;
        db = DBHandler.getInstance(context);
        this.sharedPref = context.getSharedPreferences(APP_PREF, Activity.MODE_PRIVATE);
        this.prefEditor = sharedPref.edit();
    }

    private void saveKey(String key, String value) {
        prefEditor.putString(key, value).commit();
    }

    private String getPrefString(String key) {
        return sharedPref.getString(key, "");
    }


    public void saveEntry(Entry item) {
        db.addWins(item);
    }

    public void saveNewEntry(List <Entry> itemList) {
        db.addNewWins(itemList);
    }


    public List<Entry> getEntryList() {
        List<Entry> entryList = db.getAllEntries(null);
        return entryList;
    }

    public void deleteEntry(String id) {
        db.deleteItem(id);
    }

}
