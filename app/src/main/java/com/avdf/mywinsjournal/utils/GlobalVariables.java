package com.avdf.mywinsjournal.utils;

/**
 * Created by Angelhia on 13/6/2017.
 */

public class GlobalVariables {

    public final static String INTENT_EXTRA_ENTRY = "extra_entry";
    public static String SAVED_PATH = "/MyWinsJournal/export";
    public static String FILE_NAME = "MyWinsJournal";

    public static String FILE_HEADER = "title,wins,update_date,created_date";
}
