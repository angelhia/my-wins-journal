package com.avdf.mywinsjournal.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;

import com.avdf.mywinsjournal.R;

/**
 * Created by Angelhia on 12/6/2017.
 */

public class DialogHelper {

    private static ProgressDialog progressDialog;
    private static AlertDialog alertDialog;


    /**
     * @param context
     * @param message
     */
    public static void showProgressDialog(Context context, String message) {
        progressDialog = new ProgressDialog(context, R.style.Dialog);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public static void dismissPogressDialog() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
        }

    }

    /**
     * @param context
     * @param message
     */
    public static void showOkayDialog(Context context, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context, R.style.MyDialogTheme);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(true)
                .setNegativeButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void dismissDialog() {
        if (alertDialog != null) {
            if (alertDialog.isShowing())
                alertDialog.dismiss();
        }
    }

    /**
     * @param context
     * @param title
     * @param message
     * @param positive
     */
    public static void showOkayDialog(Context context, String title, String message, DialogInterface.OnClickListener positive) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context, R.style.MyDialogTheme);
        alertDialogBuilder
                .setTitle(title)
                .setNegativeButton(context.getResources().getString(R.string.label_cancel), null)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.label_ok, positive);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}

