package com.avdf.mywinsjournal;

/**
 * Created by Angelhia on 17/4/2017.
 */


import android.app.Application;
import android.os.SystemClock;

import com.facebook.stetho.Stetho;

import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        loadCustomFont();

        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    }

    private void loadCustomFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/HelveticaNeue-Light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}