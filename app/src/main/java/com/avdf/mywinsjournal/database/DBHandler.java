package com.avdf.mywinsjournal.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.avdf.mywinsjournal.model.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angelhia on 5/6/2017.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static DBHandler mInstance;
    private Context context;

    // Database Version
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "wins_journal";

    //tables
    private static final String TABLE_WINS = "journal";

    //columns
    private static final String KEY_ID = "_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_WINS = "wins";
    private static final String KEY_DATE = "date";
    private static final String KEY_CREATED_DATE = "created_date";
    private static final String CREATE_TABLE_WINS =
            "CREATE TABLE " + TABLE_WINS + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT," + KEY_WINS + " TEXT," + KEY_DATE + " TEXT," + KEY_CREATED_DATE + " TEXT" + ")";

    private static final String ALTER_TABLE_WINS = "ALTER TABLE "
            + TABLE_WINS + " ADD COLUMN " + KEY_CREATED_DATE + " string;";

    public static synchronized DBHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBHandler(context.getApplicationContext());
        }
        return mInstance;
    }

    private DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_WINS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            sqLiteDatabase.execSQL(ALTER_TABLE_WINS);
            addCreatedDateToOldRecord(getAllEntries(sqLiteDatabase), sqLiteDatabase);
        }
        /**if (oldVersion < 3) {
         sqLiteDatabase.execSQL(V3);
         } **/
    }


    public void addWins(Entry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        if (entry.getId() != "")
            values.put(KEY_ID, entry.getId());
        values.put(KEY_TITLE, entry.getTitle());
        values.put(KEY_WINS, entry.getWins());
        values.put(KEY_DATE, entry.getDate());
        values.put(KEY_CREATED_DATE, entry.getCreatedDate());
        db.insertWithOnConflict(TABLE_WINS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void addNewWins(List<Entry> entryList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        for (int i = 0; i < entryList.size(); i++) {
            Entry entry = entryList.get(i);
            values.put(KEY_TITLE, entry.getTitle());
            values.put(KEY_WINS, entry.getWins());
            values.put(KEY_DATE, entry.getDate());
            values.put(KEY_CREATED_DATE, entry.getCreatedDate());
            db.insertWithOnConflict(TABLE_WINS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void addCreatedDateToOldRecord(List<Entry> entryList, SQLiteDatabase db) {
        db.beginTransaction();
        ContentValues values = new ContentValues();
        for (int i = 0; i < entryList.size(); i++) {
            Entry entry = entryList.get(i);
            if (entry.getId() != "")
                values.put(KEY_ID, entry.getId());
            values.put(KEY_TITLE, entry.getTitle());
            values.put(KEY_WINS, entry.getWins());
            values.put(KEY_DATE, entry.getDate());
            values.put(KEY_CREATED_DATE, entry.getDate());
            db.insertWithOnConflict(TABLE_WINS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }


    /**
     * Get all Journal Entries from the beginning of time
     */
    public List<Entry> getAllEntries(SQLiteDatabase db) {
        List<Entry> list = new ArrayList<>();
        String selectQuery = "SELECT * FROM (SELECT * FROM '" + TABLE_WINS + "'  ORDER BY " + KEY_DATE + " DESC ) ORDER BY " + KEY_CREATED_DATE + " DESC";
        Log.d("select query", selectQuery);
        if (db == null)
            db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    Entry reporting = new Entry();
                    reporting.setId(cursor.getString(0));
                    reporting.setTitle(cursor.getString(1));
                    reporting.setWins(cursor.getString(2));
                    reporting.setDate(cursor.getString(3));
                    reporting.setCreatedDate(cursor.getString(4));
                    list.add(reporting);
                } while (cursor.moveToNext());
            }

        } finally {
            cursor.close();
        }

        return list;
    }

    /**
     * Reset table
     */
    public void deleteItem(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + TABLE_WINS + " WHERE " + KEY_ID + "='" + id + "'");
    }

}
