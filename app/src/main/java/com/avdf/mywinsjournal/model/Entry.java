package com.avdf.mywinsjournal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Angelhia on 12/6/2017.
 */

public class Entry implements Serializable {

    private String id;
    private String title;
    private String date;
    private String wins;
    private String createdDate;


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }
}
